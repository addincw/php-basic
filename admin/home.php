<?php
session_start();
$title = 'Dashboard';
require_once '../layouts/admin/header.php';

if( empty($_SESSION['user']) && empty($_SESSION['role']) )
{
    header('Location: admin/login.php');
}

require_once '../models/User.php';

$user = new User();
$users = $user->all();
?>
<!-- hero page -->
<section class="hero">
  <div class="hero-body">
    <div class="container">
      <h1 class="title">
        Dashboard
      </h1>
      <h2 class="subtitle">
        Hai, selamat datang <?= $_SESSION['user']->name ?>
      </h2>
    </div>
  </div>
</section>

<!-- content -->
<section class="section">
    <div class="container">
        <table class="table is-bordered is-hoverable" width="50%;">
            <thead>
                <th>No</th>
                <th>Name</th>
                <th>Action</th>
            </thead>
            <tbody>
                <?php 
                $no = 1;
                foreach ($users as $user) : 
                ?>
                <tr>
                    <td><?= $no ?></td>
                    <td><?= $user->name ?></td>
                    <td>
                        <p class="buttons">
                            <a class="button is-small is-info" href="update.php?id=<?= $user->id ?>">
                                <span class="icon is-small">
                                <i class="fa fa-pencil"></i>
                                </span>
                            </a>
                            <a class="button is-small is-danger" href="../actions.php?type=delete&id=<?= $user->id ?>">
                                <span class="icon is-small">
                                <i class="fa fa-trash"></i>
                                </span>
                            </a>
                    </td>
                </tr>
                <?php 
                $no++;
                endforeach; 
                ?>
            </tbody>
        </table>
    </div>
</section>

<?php
require_once '../layouts/footer.php';
?>