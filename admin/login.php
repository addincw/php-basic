<?php
session_start();

if( !empty($_SESSION['user']) )
{
    header('Location: ./home.php');
}

$title = 'Admin | Login';
require_once '../layouts/admin/header.php';
?>

<!-- hero page -->
<section class="hero">
  <div class="hero-body">
    <div class="container">
      <h1 class="title">
        Login Admin
      </h1>
      <h2 class="subtitle">
        Gunakan username / email yang sudah anda daftarkan.
      </h2>
    </div>
  </div>
</section>

<!-- content -->
<section class="section">
    <div class="container">
        <form action="../actions.php" method="post" style="width:50%">
            <input type="hidden" name="type" value="login">
            <input type="hidden" name="admin" value="true">

            <div class="field">
                <label class="label">Username</label>
                <div class="control">
                    <input class="input" name="username" type="text" placeholder="e.g Alex Smith">
                </div>
            </div>

            <div class="field">
                <label class="label">Email</label>
                <div class="control">
                    <input class="input" name="email" type="email" placeholder="e.g Alex@gmail.com">
                </div>
            </div>

            <div class="field">
                <label class="label">Password</label>

                <div class="field has-addons">
                    <div class="control">
                        <input id="fieldPwd" class="input" name="password" type="password">
                    </div>

                    <p class="control">
                        <a id="showPwd" class="button"> lihat </a>
                    </p>
                </div>
            </div>

            <!-- <div>
                <p>Belum punya akun ? <a href="index.php">Daftar</a></p>
            </div> -->

            <div class="field" style="margin-top: 3rem;">
                <div class="control">
                    <button class="button is-primary is-fullwidth" type="submit"> Submit </button>
                </div>
            </div>
        </form>
    </div>
</section>

<?php
require_once '../layouts/footer.php';
?>