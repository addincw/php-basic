<?php
session_start();
require_once '../models/User.php';

$title = 'Update User';
require_once '../layouts/admin/header.php';

$user = new User();
// get user by id
$user = $user->byId($_REQUEST['id']);
?>

<!-- hero page -->
<section class="hero">
  <div class="hero-body">
    <div class="container">
      <h1 class="title">
        Update User <?= $user->name ?>
      </h1>
      <h2 class="subtitle">
        Perbarui data user.
      </h2>
    </div>
  </div>
</section>

<!-- content -->
<section class="section">
    <div class="container">
        <form action="../actions.php" method="post" style="width:50%">
            <input type="hidden" name="type" value="update">
            <input type="hidden" name="id" value="<?= $user->id ?>">

            <div class="field">
                <label class="label">Nama Lengkap</label>
                <div class="control">
                    <input class="input" name="name" type="text" placeholder="e.g Alex Smith" required value="<?= isset($oldField['name']) ? $oldField['name'] : $user->name ?>">
                </div>
            </div>

            <div class="field">
                <label class="label">Username</label>
                <div class="control">
                    <input class="input <?php if(isset($messageField['username'])){ echo 'is-danger'; } ?>" name="username" type="text" placeholder="e.g Alex Smith" required value="<?= isset($oldField['username']) ? $oldField['username'] : $user->username  ?>">
                </div>

                <?php if(isset($messageField['username'])): ?>
                <p class="help is-danger">
                    <?= $messageField['username'] ?>
                </p>
                <?php endif; ?>
            </div>

            <div class="field">
                <label class="label">Email</label>
                <div class="control">
                    <input class="input <?php if(isset($messageField['email'])){ echo 'is-danger'; } ?>" name="email" type="email" placeholder="e.g Alex@gmail.com" required value="<?= isset($oldField['email']) ? $oldField['email'] : $user->email  ?>">
                </div>

                <?php if(isset($messageField['email'])): ?>
                <p class="help is-danger">
                    <?= $messageField['email'] ?>
                </p>
                <?php endif; ?>
            </div>

            <div class="field" style="margin-top: 3rem;">
                <div class="control">
                    <button class="button is-primary is-fullwidth" type="submit"> Submit </button>
                </div>
            </div>
        </form>
    </div>
</section>

<?php require_once '../layouts/footer.php'; ?>
