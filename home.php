<?php
session_start();
$title = 'Dashboard';
require_once 'layouts/header.php';

if( empty($_SESSION['user']) )
{
    header('Location: login.php');
}
?>
<!-- hero page -->
<section class="hero">
  <div class="hero-body">
    <div class="container">
      <h1 class="title">
        Dashboard
      </h1>
      <h2 class="subtitle">
        Hai, selamat datang <?= $_SESSION['user']->name ?>
      </h2>
    </div>
  </div>
</section>

<?php
require_once 'layouts/footer.php';
?>