<?php
session_start();
require_once "Models\User.php";

$user = new User ();

$action = $_REQUEST['type'];

switch ($action) {
    case 'login':
        $user = $user->authenticate([
            'username' => $_REQUEST['username'],
            'email' => $_REQUEST['email'],
            'password' => $_REQUEST['password']
        ]);

        if($user){
            $_SESSION['user'] = $user;
            
            if($_REQUEST['admin']){
                $_SESSION['role'] = 'admin';
                header('Location: admin/home.php');
                break;
            }
            
            header('Location: home.php');
            break;
        }

        $_SESSION['notif']['type'] = 'is-danger';
        $_SESSION['notif']['message'] = 'username / password tidak sesuai.';

        if($_REQUEST['admin']){
            header('Location: admin/login.php');
            break;
        }
        
        header('Location: login.php');
        break;

    case 'logout':
        unset($_SESSION['user']);

        header('Location: login.php');
        break;

    case 'insert':
        // validate form
        if(! validateForm($_REQUEST) ) {
            $_SESSION['notif']['type'] = 'is-danger';
            $_SESSION['notif']['message'] = $result . ' periksa ulang form.';

            $_SESSION['old'] = $_REQUEST;            
            header('Location: index.php');
            break;
        }

        // insert user
        $result = $user->insert([
            'name' => $_REQUEST['name'],
            'username' => $_REQUEST['username'],
            'email' => $_REQUEST['email'],
            'password' => $_REQUEST['password']
        ]);

        if ($result == 1){
            $_SESSION['notif']['type'] = 'is-success';
            $_SESSION['notif']['message'] = $result . ' data berhasil di tambahkan.';
            
            header('Location: login.php');
            break;
        }
        
        $_SESSION['notif']['type'] = 'is-danger';
        $_SESSION['notif']['message'] = 'data gagal di tambahkan.';

        header('Location: index.php');    
        break;
    
    case 'update':
        // validate form
        if(! validateForm($_REQUEST, 'update') ) {
            $_SESSION['notif']['type'] = 'is-danger';
            $_SESSION['notif']['message'] = $result . ' periksa ulang form.';

            $_SESSION['old'] = $_REQUEST;            
            header('Location: admin/update.php?id='.$_REQUEST['id']);
            break;
        }
        // update user by id
        $result = $user->updateById($_REQUEST['id'], [
            'name' => $_REQUEST['name'],
            'username' => $_REQUEST['username'],
            'email' => $_REQUEST['email']
        ]);

        if ($result == 1){
            $_SESSION['notif']['type'] = 'is-success';
            $_SESSION['notif']['message'] = $result . ' data berhasil di perbarui.';

            header('Location: admin/home.php');
            break;
        }

        $_SESSION['notif']['type'] = 'is-danger';
        $_SESSION['notif']['message'] = 'data gagal di perbarui.';

        header('Location: create.php'); 
        break;

    case 'delete':
        // delete user by id
        $user->deleteById($_REQUEST['id']);

        $_SESSION['notif']['type'] = 'is-success';
        $_SESSION['notif']['message'] = 'data berhasil di hapus.';

        header('Location: admin/home.php');
        break;
}


function validateForm ($params = [], $action='insert')
{
    $isValidated = true;
    
    // is username exist
    $user = new User ();
    $userByUsername = $user->byUsername($params['username']);
    if ( ! empty($userByUsername) ){
        if( empty($params['id']) || $userByUsername->id != $params['id'] ){
            $_SESSION['notif']['messageField']['username'] = "username sudah ada yang menggunakan";
            $isValidated = false;
        }

    }
    // is Email exist
    $user = new User ();
    $userByEmail = $user->byEmail($params['email']);
    if ( ! empty($userByEmail) ){
        if( empty($params['id']) || $userByEmail->id != $params['id'] ){
            $_SESSION['notif']['messageField']['email'] = "email sudah ada yang menggunakan";
            $isValidated = false;
        }

    }
    // if registration
    if ($action === 'insert') {
        // is captcha right
        if( isset($_SESSION['Captcha']) &&  $_SESSION['Captcha'] != $params['captcha'] ){
            $_SESSION['notif']['messageField']['captcha'] = "captcha tidak sesuai";
            $isValidated = false;        
        }
        // password at least 8 character
        if( strlen($params['password']) < 8 ){
            $_SESSION['notif']['messageField']['password'] = "password minimal 8 karakter";
            $isValidated = false;
        }
    }

    return $isValidated;
}