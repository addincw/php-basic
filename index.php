<?php
session_start();
$title = 'Registrasi';
require_once 'layouts/header.php';

if( !empty($_SESSION['user']) )
{
    header('Location: home.php');
}
?>

<!-- hero page -->
<section class="hero">
  <div class="hero-body">
    <div class="container">
      <h1 class="title">
        Registrasi
      </h1>
      <h2 class="subtitle">
        Isi dengan lengkap, dan sesuai dengan data anda.
      </h2>
    </div>
  </div>
</section>

<!-- content -->
<section class="section">
    <div class="container">
        <form action="actions.php" method="post" style="width:50%">
            <input type="hidden" name="type" value="insert">

            <div class="field">
                <label class="label">Nama Lengkap</label>
                <div class="control">
                    <input class="input" name="name" type="text" placeholder="e.g Alex Smith" required value="<?php if(isset($oldField['name'])){ echo $oldField['name']; } ?>">
                </div>
            </div>

            <div class="field">
                <label class="label">Username</label>
                <div class="control">
                    <input class="input <?php if(isset($messageField['username'])){ echo 'is-danger'; } ?>" name="username" type="text" placeholder="e.g Alex Smith" required value="<?php if(isset($oldField['username'])){ echo $oldField['username']; } ?>">
                </div>

                <?php if(isset($messageField['username'])): ?>
                <p class="help is-danger">
                    <?= $messageField['username'] ?>
                </p>
                <?php endif; ?>
            </div>

            <div class="field">
                <label class="label">Email</label>
                <div class="control">
                    <input class="input <?php if(isset($messageField['email'])){ echo 'is-danger'; } ?>" name="email" type="email" placeholder="e.g Alex@gmail.com" required value="<?php if(isset($oldField['email'])){ echo $oldField['email']; } ?>">
                </div>

                <?php if(isset($messageField['email'])): ?>
                <p class="help is-danger">
                    <?= $messageField['email'] ?>
                </p>
                <?php endif; ?>
            </div>

            <div class="field">
                <label class="label">Captcha</label>
                <div class="control">
                    <img src="captcha.php" alt="gambar" />
                </div>

                <input class="input <?php if(isset($messageField['captcha'])){ echo 'is-danger'; } ?>" type="text" name='captcha'>

                <?php if(isset($messageField['captcha'])): ?>
                <p class="help is-danger">
                    <?= $messageField['captcha'] ?>
                </p>
                <?php endif; ?>
            </div>

            <div class="field">
                <label class="label">Password</label>

                <div class="field has-addons">
                    <div class="control">
                        <input id="fieldPwd" class="input <?php if(isset($messageField['password'])){ echo 'is-danger'; } ?>" name="password" type="password" value="<?php if(isset($oldField['password'])){ echo $oldField['password']; } ?>">
                    </div>

                    <p class="control">
                        <a id="showPwd" class="button"> lihat </a>
                    </p>
                </div>

                <?php if(isset($messageField['password'])): ?>
                <p class="help is-danger">
                    <?= $messageField['password'] ?>
                </p>
                <?php else: ?>
                <p class="help is-info">
                    Password minimal 8 karakter
                </p>
                <?php endif; ?>
            </div>


            <div class="field" style="margin-top: 3rem;">
                <div class="control">
                    <button class="button is-primary is-fullwidth" type="submit"> Submit </button>
                </div>
            </div>

            <div>
                <p>Sudah punya akun ? <a href="login.php">Login</a></p>
            </div>
        </form>
    </div>
</section>

<?php
require_once 'layouts/footer.php';
?>