<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?= $title ?></title>
    <!-- css -->
    <link rel="stylesheet" href="../src/bulma-0.7.5/css/bulma.min.css">
    <link rel="stylesheet" href="../src/font-awesome-4.7.0/css/font-awesome.min.css">
</head>
<body>
<nav class="navbar" role="navigation" aria-label="main navigation" style="border-bottom: 1px solid lightgrey;">
  <div class="container">
    <div class="navbar-brand">
        <a class="navbar-item" href="https://bulma.io">
            <img src="https://bulma.io/images/bulma-logo.png" alt="Bulma: Free, open source, & modern CSS framework based on Flexbox" width="112" height="28">
        </a>
    </div>

    <div class="navbar-end">
      <div class="navbar-item">
        <div class="buttons">
          <?php if(!empty($_SESSION['user'])): ?>
            <a class="button is-light" href="../actions.php?type=logout">
              Logout
            </a>
          <?php else: ?>
            <!-- <a class="button is-primary">
              <strong>Daftar</strong>
            </a>
            <a class="button is-light">
              <strong>Login</strong>
            </a> -->
          <?php endif; ?>
        </div>
      </div>
    </div>

  </div>  
</nav>

<?php if( isset($_SESSION['notif']) ) : ?>
    <div id="notif" class="notification <?= $_SESSION['notif']['type'] ?>" style="position: absolute; top: 65px; right: 10px;">
        <button id="notifBtn" class="delete"></button>
        <?= $_SESSION['notif']['message'] ?>
    </div>

    <script type="text/javascript">
    setTimeout(() => {
        if(document.getElementById('notif'))
            document.getElementById('notif').remove();
    }, 2000);
    
    var notifBtn = document.getElementById('notifBtn');

    notifBtn.addEventListener('click', function () {
        document.getElementById('notif').remove();
    })
    </script>
<?php
    if(isset($_SESSION['old']))
      $oldField = $_SESSION['old'];

    if(isset($_SESSION['notif']['messageField']))  
      $messageField = $_SESSION['notif']['messageField'];
      
    unset($_SESSION['notif']);
endif;
?>