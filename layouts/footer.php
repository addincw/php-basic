<script type="text/javascript">
    var btnShowPwd = document.getElementById('showPwd');
    var fieldPwd = document.getElementById('fieldPwd')

    var isShowPwd = 0

    btnShowPwd.addEventListener('click', function () {
        if(isShowPwd == 0) {
            btnShowPwd.innerHTML = 'Sembunyikan'
            fieldPwd.type = 'text'
            isShowPwd = 1   
            return
        }
        
        btnShowPwd.innerHTML = 'Lihat'
        fieldPwd.type = 'password'
        isShowPwd = 0   
        return    
    })
</script>
</body>
</html>