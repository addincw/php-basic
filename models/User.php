<?php

class User
{
    protected $table = 'users';
    // create connection
    public function __construct ()
    {
        try {
            $this->dbh = new PDO('mysql:host=localhost;dbname=db-test-kominfo', "root", "");
            // set error mode
            $this->dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            echo 'oops : ';
            echo '<br>';
            
            echo $e->getMessage();
            return;
        }
    }
    public function all ()
    {
        $result = [];
        $query = $this->dbh->query("SELECT * FROM {$this->table}");

        while( $row = $query->fetch(PDO::FETCH_OBJ) )
            $result[] = $row;

        return $result;
    }
    public function byId ($id)
    {
        $result = [];
        $query = $this->dbh->query("SELECT * FROM {$this->table} where id = {$id}");

        while( $row = $query->fetch(PDO::FETCH_OBJ) )
            $result[] = $row;

        return $result[0];
    }
    public function byUsername ($username)
    {
        $result = [];
        $query = $this->dbh->query("SELECT * FROM {$this->table} where username = '{$username}'");

        while( $row = $query->fetch(PDO::FETCH_OBJ) )
            $result[] = $row;

        return $result[0];
    }
    public function byEmail ($email)
    {
        $result = [];
        $query = $this->dbh->query("SELECT * FROM {$this->table} where email = '{$email}'");

        while( $row = $query->fetch(PDO::FETCH_OBJ) )
            $result[] = $row;

        return $result[0];
    }
    // insert new data
    public function insert ($params = [])
    {
        //hashing password
        $password = password_hash($params['password'], PASSWORD_DEFAULT, [ 'cost' => 10 ]);

        try {
            // prepare query statement
            $stmt = $this->dbh->prepare("INSERT INTO {$this->table} VALUES (
                :id, 
                :name, 
                :email,
                :password,
                :username
            )");
            // bind and execute statement
            $stmt->execute([
                ':id' => NULL,
                ':name' => $params['name'],
                ':email' => $params['email'],
                ':password' => $password,
                ':username' => $params['username']
            ]);
        } catch (PDOException $e) {
            throw $e;            
            return;
        }

        // if success return 1
        return $stmt->rowCount();
    }
    // update by id
    public function updateById ($id, $params = [])
    {
        try {
            $stmt = $this->dbh->prepare("UPDATE {$this->table} SET 
                name = :name, 
                username = :username,
                email = :email
            where id = {$id}");
            $stmt->execute([
                ':name' => $params['name'],
                ':username' => $params['username'],
                ':email' => $params['email']
            ]);
        } catch (PDOException $e) {
            throw $e;            
            return;
        }

        // if success return 1
        // return $stmt->rowCount();
        return 1;
    }
    // delete by id
    public function deleteById ($id)
    {
        try {
            $stmt = $this->dbh->prepare("DELETE FROM {$this->table} where id = {$id}");
            $stmt->execute();
        } catch (PDOException $e) {
            throw $e;            
            return;
        }

        // if success return 1
        return $stmt->rowCount();
    }
    // authenticate user
    public function authenticate($params = [])
    {
        $username = $_REQUEST['username'];
        $email = $_REQUEST['email'];
        $password = $_REQUEST['password'];

        $result = [];
        // check username and email is exist 
        $query = $this->dbh->query("SELECT * FROM {$this->table} WHERE username = '{$username}' and email = '{$email}'");

        while( $row = $query->fetch(PDO::FETCH_OBJ) ){
            $result[] = $row;
        }

        if( !empty($result[0]) ){
            $user = $result[0];

            if ( password_verify($password, $user->password) ) {
                return $user;
            }
            
            return false;
        };

        return false;
    }
}
